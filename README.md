---
margin-left: 2cm
margin-right: 2cm
margin-top: 1cm
margin-bottom: 2cm
title: 김진섭
description-meta: '원격 근무, 개발자'
subject: '이력서'
pdf: https://gitlab.com/vmfhrmfoaj/resume/-/jobs/artifacts/master/raw/resume.pdf?job=upload
---
###### [[홈페이지](https://jinseop.kim)] . [[링크드인](https://www.linkedin.com/in/jinseop-kim-127b3480/)]


## Skill

**Programming languages**: `C`, `Go`, `Perl`  
**Database**: `SQLite`, `MySQL`  
**Linux distributions**: `CentOS`, `Gentoo`  
**Tools**: `Ansible`, `Git`


## Experience

### Research & Development Engineer, ANTlabs

Oct. 2017 – Present

주로 시스템 및 기타 다른 프로그램들의 설정 파일을 관리하는 프로그램을 유지보수 하며, 아래와 같은 일들을 수행했습니다:

- Nginx 모듈:
  Nginx에서 Squid의 URL rewritor 스크립트를 실행하고 그 결과를 요청에 반영합니다.  
  C 언어로 비동기 프로그래밍은 처음이었지만 Nginx가 잘 설계 되어서 그런지 생각보다 수월하게 개발하였습니다.
  훗날 저도 잘 설계된 오픈소스 프로젝트를 만들어 다른 개발자들에게 유사한 경험을 돌려주고 싶은 즐거운 경험이었습니다.

<!-- tight list -->

- (OpenWRT에서) TLS CLIENT-HELLO의 SNI 파싱해 사용자가 접속한 사이트를 기록하는 커널 모듈 개발.
- DHCP 서버 통계를 제공하는 SNMP 에이전트 개발.
- ETSI E2 인터페이스의 일부분만 제공하는 FreeDiameter 모듈 개발.
- Kea에 사용자 정의 DHCPv6 주소 할당자 추가.
- KeyCloak의 User Federation (UserStroage SPI) 모듈 개발.
- PHP에서 지원하지 않는 시스템 호출을 사용할 수 있도록 PHP 모듈 개발.
- UI 팀을 위해 PHP 언어로 SAML 인증하는 가이드 코드 작성.
- 서로 다른 네트워크에 있는 크롬캐스트와 인증받은 사용자 간의 통신을 위한 프록시 개발.
- 추가 DNS 검색없이 패킷의 기존 목적지 주소를 그대로 사용하도록 sniproxy 수정.
- 협력사의 도메인 분류 데이터를 변환 및 배포, 적용하는 프로그램 개발.

*Technologies used:* `Diameter`, `Linux`, `DHCP`, `mDNS/DNS-SD`, `Nginx module`, `SMAL`, `SNMP`


### Assistant Engineer, SDC Micro

Dec. 2012 – Aug. 2016

퀄컴 카메라 팀에 파견되어서 장비업체와 카메라 센서를 제조하는 공정에서 사용할 QA 소프트웨어 개발 및 유지보수했습니다.

*Technologies used:* `Linux device driver`


## Personal Project

- [**event-handler**](https://gitlab.com/vmfhrmfoaj/event-handler):
리눅스를 사용하면서 겪는 사소한 불편함들을 해소하기 위해 D-Bus에서 특정 이벤트가 발생했을 때 지정된 스크립트나 프로그램을 실행시키는 프로그램입니다.

*Technologies used:* `D-Bus`


## Education

### 호서대학교 일반대학원 컴퓨터공학 석사

Mar. 2011 – Feb. 2013

### 호서대학교 컴퓨터공학 학사

Mar. 2011 – Feb. 2013
