FROM ubuntu:24.04

ENV INSIDE_CONTAINER=docker;ubuntu:24.04

RUN sed -i 's@URIs:.*$@URIs: http://pkg.jinseop.kim/repository/ubuntu/@g' /etc/apt/sources.list.d/ubuntu.sources && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt update && apt -y upgrade && \
    apt -y install pandoc fonts-noto fonts-noto-cjk wget make && \
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt install -fy ./google-chrome-stable_current_amd64.deb && \
    apt-get clean && \
    rm -f google-chrome-stable_current_amd64.deb

WORKDIR /workspace
USER ubuntu
CMD [ "bash" ]
