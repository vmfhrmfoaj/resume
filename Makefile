DOCKER_IMG ?= vmfhrmfoaj/pandoc:4

ifdef INSIDE_CONTAINER
PANDOC ?= pandoc
else
PANDOC ?= docker run --rm --interactive -v $(CURDIR):/workspace $(DOCKER_IMG) pandoc
endif

.PHONY: help
help: ## List available Make commands (Make targets with PHONY)
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

all: html pdf  ## Generate resume in html and pdf format

.PHONY: html
html: resume.html  ## Generate resume in html format

.PHONY: pdf
pdf: resume.pdf  ## Generate resume in pdf format

.PHONY: clean
clean:  ## Remove generated files
	rm -f resume.pdf
	rm -f resume.html


resume.pdf: resume.html
	google-chrome --headless --no-gpu --no-sandbox --print-to-pdf=$@ --no-pdf-header-footer --window-size=1920,1080 $<

resume.html: README.md normalize.css
	$(PANDOC) $< -f markdown -c normalize.css -t html -s -o $@
